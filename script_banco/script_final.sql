CREATE SCHEMA `dbteste` ;

CREATE TABLE `clientes` (
  `id_cliente` int NOT NULL AUTO_INCREMENT,
  `nome_cli` varchar(100) DEFAULT NULL,
  `cidade_cli` varchar(50) DEFAULT NULL,
  `uf_cli` char(2) DEFAULT NULL,
  PRIMARY KEY (`id_cliente`),
  KEY `IDX_NOME_CLI` (`nome_cli`) /*!80000 INVISIBLE */
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


CREATE TABLE `pedido` (
  `id_pedido` int NOT NULL,
  `id_cliente` int DEFAULT NULL,
  `data_emissao` date DEFAULT NULL,
  `valor_total` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`id_pedido`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


CREATE TABLE `pedido_item` (
  `id_pedido` int NOT NULL,
  `id_item` int NOT NULL AUTO_INCREMENT,
  `id_produto` int DEFAULT NULL,
  `qtde` int DEFAULT NULL,
  `valor_unit` decimal(10,2) DEFAULT NULL,
  `valor_total` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`id_item`,`id_pedido`),
  KEY `FK_PEDITM_PED_idx` (`id_pedido`),
  KEY `FK_PEDITM_PROD_idx` (`id_produto`),
  CONSTRAINT `FK_PEDITM_PED` FOREIGN KEY (`id_pedido`) REFERENCES `pedido` (`id_pedido`),
  CONSTRAINT `FK_PEDITM_PROD` FOREIGN KEY (`id_produto`) REFERENCES `produtos` (`id_produto`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


CREATE TABLE `produtos` (
  `id_produto` int NOT NULL AUTO_INCREMENT,
  `nome_prod` varchar(100) DEFAULT NULL,
  `preco_prod` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`id_produto`),
  KEY `IDX_NOME_PROD` (`nome_prod`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
  
  
 

insert into clientes (nome_cli, cidade_cli, uf_cli) values ('marcel duarte', 'Cuiab�', 'MT');
insert into clientes (nome_cli, cidade_cli, uf_cli) values ('fabiana duarte', 'Varzea Grande', 'MT');
insert into clientes (nome_cli, cidade_cli, uf_cli) values ('rhuan marcel', 'Cuiab�', 'MT');
insert into clientes (nome_cli, cidade_cli, uf_cli) values ('geovana santini', 'Varzea Grande', 'MT');
insert into clientes (nome_cli, cidade_cli, uf_cli) values ('nicole santini', 'S�o Paulo', 'SP');
insert into clientes (nome_cli, cidade_cli, uf_cli) values ('lili da sila', 'S�o Paulo', 'SP');
insert into clientes (nome_cli, cidade_cli, uf_cli) values ('neuza maria', 'Curitiba', 'PR');
insert into clientes (nome_cli, cidade_cli, uf_cli) values ('antonio bandeiras', 'Curitiba', 'PR');
insert into clientes (nome_cli, cidade_cli, uf_cli) values ('nicole kidman', 'Curitiba', 'PR');
insert into clientes (nome_cli, cidade_cli, uf_cli) values ('tom cruise', 'Cuiab�', 'MT');
insert into clientes (nome_cli, cidade_cli, uf_cli) values ('tom rolland', 'Cuiab�', 'MT');
insert into clientes (nome_cli, cidade_cli, uf_cli) values ('jack chan', 'Porto Velho', 'RO');
insert into clientes (nome_cli, cidade_cli, uf_cli) values ('john travolta', 'Porto Velho', 'RO');
insert into clientes (nome_cli, cidade_cli, uf_cli) values ('jo�o roberto', 'Manaus', 'AM');
insert into clientes (nome_cli, cidade_cli, uf_cli) values ('jo�o roberto', 'Manaus', 'AM');
insert into clientes (nome_cli, cidade_cli, uf_cli) values ('john lennon', 'Rio de Janeiro', 'RJ');
insert into clientes (nome_cli, cidade_cli, uf_cli) values ('paul mccartney', 'Rio de Janeiro', 'RJ');
insert into clientes (nome_cli, cidade_cli, uf_cli) values ('ringo star', 'Belo Horizonte', 'MG');
insert into clientes (nome_cli, cidade_cli, uf_cli) values ('george harrison', 'Belo Horizonte', 'MG');
insert into clientes (nome_cli, cidade_cli, uf_cli) values ('ev�dio fraga', 'Belo Horizonte', 'MG');


insert into produtos (nome_prod, preco_prod) values ('mouse sem fio', 100.50);
insert into produtos (nome_prod, preco_prod) values ('teclado sem fio', 80.00);
insert into produtos (nome_prod, preco_prod) values ('monitor 21 pol', 700.00);
insert into produtos (nome_prod, preco_prod) values ('cabo coaxial metro', 3.20);
insert into produtos (nome_prod, preco_prod) values ('lapizeira 5.0', 22.70);
insert into produtos (nome_prod, preco_prod) values ('mousepad preto', 10.00);
insert into produtos (nome_prod, preco_prod) values ('mousepad cinza', 10.00);
insert into produtos (nome_prod, preco_prod) values ('celular motorola', 850.00);
insert into produtos (nome_prod, preco_prod) values ('celular xiaomi', 950.00);
insert into produtos (nome_prod, preco_prod) values ('celular samsung', 1000.00);
insert into produtos (nome_prod, preco_prod) values ('celular nokia', 750.00);
insert into produtos (nome_prod, preco_prod) values ('notebook 15 pol i5 8gb dell', 2799.00);
insert into produtos (nome_prod, preco_prod) values ('notebook 17 pol i7 16gb dell', 4999.00);
insert into produtos (nome_prod, preco_prod) values ('notebook 15 pol i3 4gb acer', 2999.00);
insert into produtos (nome_prod, preco_prod) values ('tv 43 pol', 2700.00);
insert into produtos (nome_prod, preco_prod) values ('tv 45 pol', 3100.00);
insert into produtos (nome_prod, preco_prod) values ('tv 55 pol', 3800.00);
insert into produtos (nome_prod, preco_prod) values ('cama box solteiro', 700.00);
insert into produtos (nome_prod, preco_prod) values ('cama box casal', 1700.00);
insert into produtos (nome_prod, preco_prod) values ('cama box solteiro', 700.00);
