unit mdlPedidoItem;

interface

type
  TPedidoItem = class

  private
    FID_Pedido: integer;
    FID_Item: integer;
    FID_Produto: integer;
    FNome_Prod: string;
    FQtde: integer;
    FValor_Unit: Double;
    FValor_Total: Double;

  public
    property id_pedido: integer read FID_Pedido write FID_Pedido;
    property id_item: integer read FID_Item write FID_Item;
    property id_produto: integer read FID_Produto write FID_Produto;
    property nome_prod: string read FNome_Prod write FNome_Prod;
    property qtde: integer read FQtde write FQtde;
    property valor_unit: double read FValor_Unit write FValor_Unit;
    property valor_total: double read FValor_Total write FValor_Total;


// so pra ter uma referencia - nao tera funcoes ou ser no controler

    //procedure ExisteCliente(Nome:String) ;virtual;  abstract;
    //function ExisteCliente(Nome:String):Boolean;
end;


implementation



end.
