unit mdlPedido;

interface

uses System.Generics.Collections,
  mdlPedidoItem;

type
  TPedido = class

  private
    FID_Pedido: integer;
    FID_Cliente: integer;
    FNome_Cli: string;
    FData_Emissao: TDatetime;
    FValor_Total: Double;
    FListaPedidoItem: TObjectList<TPedidoItem>;

  public
    constructor Create(ListaPedidoItem: TObjectList<TPedidoItem>)  overload;
    constructor Create()  overload;

    property id_pedido: integer read FID_Pedido write FID_Pedido;
    property id_cliente: integer read FID_Cliente write FID_Cliente;
    property nome_cli: string read FNome_Cli write FNome_Cli;
    property data_emissao: TDatetime read FData_Emissao write FData_Emissao;
    property valor_total: double read FValor_Total write FValor_Total;
    //
    property listapedidoitem: TObjectList<TPedidoItem> read FListaPedidoItem
                                                  write FListaPedidoItem;

// so pra ter uma referencia - nao tera funcoes ou ser no controler

    //procedure ExisteCliente(Nome:String) ;virtual;  abstract;
    //function ExisteCliente(Nome:String):Boolean;
end;


implementation

constructor TPedido.Create(ListaPedidoItem: TObjectList<TPedidoItem>);
begin
  FListaPedidoItem := ListaPedidoItem;
end;

constructor TPedido.Create;
begin
  FListaPedidoItem := TObjectList<TPedidoItem>.Create();
end;



end.
