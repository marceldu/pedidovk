program Project1;

uses
  Vcl.Forms,
  vwPedido in 'view\vwPedido.pas' {fPedido},
  mdlPedidoItem in 'models\mdlPedidoItem.pas',
  mdlProduto in 'models\mdlProduto.pas',
  uDM in 'uDM.pas' {DM: TDataModule},
  ctrCliente in 'controllers\ctrCliente.pas',
  ctrPedido in 'controllers\ctrPedido.pas',
  mdlCliente in 'models\mdlCliente.pas',
  mdlPedido in 'models\mdlPedido.pas',
  ctrProduto in 'controllers\ctrProduto.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TfPedido, fPedido);
  Application.CreateForm(TDM, DM);
  Application.Run;
end.
