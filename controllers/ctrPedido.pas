unit ctrPedido;

interface

uses
  Data.DB,
  uDM,
  mdlPedido,
  mdlPedidoItem,
  System.Classes,
  System.SysUtils,
  //
  FireDAC.DApt,
  FireDAC.Comp.DataSet,
  //
  FireDAC.Comp.Client;

type
  TCtrPedido = class
  private
    { private declarations }
  protected
    { protected declarations }

  public
    { public declarations }
    function GetConnection:TFDConnection;
    function InserePedido(Pedido: TPedido): Boolean;
    function ExcluiPedido(PId_Pedido: Integer): Boolean;
    function ProcuraPedido(PId_Pedido: Integer): TPedido;

  published
    { published declarations }
  end;


implementation


function TCtrPedido.GetConnection: TFDConnection;
begin
    DM := TDM.Create(nil);
    Result:= DM.FDConnection;
end;

function TCtrPedido.InserePedido(Pedido: TPedido): Boolean;
var
  i, iId_Pedido: integer;
  Command: TFDCommand;
  Query: TFDQuery;
  oConnection:TFDConnection;
  ObjPedidoItem:TPedidoItem;
begin
  try
    Result              := False;
    oConnection         := GetConnection;
    Command             := TFDCommand.Create(nil);
    Command.Connection  := oConnection;

    Query := TFDQuery.Create(nil);
    Query.Connection := oConnection;
    try
      //Query.SQL.Text := 'select coalesce(max(id_pedido),0)+1 id_pedido_novo from pedido ';
      Query.Open('select coalesce(max(id_pedido),0)+1 id_pedido_novo from pedido ');
      iId_Pedido := Query.FieldByName('id_pedido_novo').AsInteger;

      oConnection.StartTransaction;
      Command.Close;
      Command.CommandText.Text:= ' insert into pedido (id_pedido, id_cliente, data_emissao, valor_total) '+
                                 ' values (:id_pedido, :id_cliente, current_date(), :valor_total)';
      Command.Params.ParamByName('id_pedido').AsInteger  := iId_Pedido;
      Command.Params.ParamByName('id_cliente').AsInteger := Pedido.id_cliente;
      Command.Params.ParamByName('valor_total').Value    := Pedido.valor_total;
      Command.Execute();
//      oConnection.Commit;
      //

      for ObjPedidoItem in Pedido.listapedidoitem do
      begin
//        oConnection.StartTransaction;
        Command.Close;
        Command.CommandText.Text:= ' insert into pedido_item (id_pedido, id_produto, qtde, valor_unit, valor_total) '+
                                   ' values (:id_pedido, :id_produto, :qtde, :valor_unit, :valor_total)';
        Command.Params.ParamByName('id_pedido').AsInteger  := iId_Pedido;
        Command.Params.ParamByName('id_produto').AsInteger := ObjPedidoItem.id_produto ;
        Command.Params.ParamByName('qtde').AsInteger       := ObjPedidoItem.qtde;
        Command.Params.ParamByName('valor_unit').AsFloat   := ObjPedidoItem.valor_unit;
        Command.Params.ParamByName('valor_total').AsFloat  := ObjPedidoItem.valor_total;
        //
        Command.Execute();
      end;
      oConnection.Commit;
      Result := True;
    except
      on E:Exception do
      begin
        oConnection.Rollback;
      end;
    end;
  finally
    Command.Free;
    Query.Free;
  end;
end;

function TCtrPedido.ExcluiPedido(PId_Pedido: Integer): Boolean;
var
  Command:TFDCommand;
  Query  :TFDQuery;
  oConnection:TFDConnection;
begin
  try
    oConnection        := GetConnection;
    Command            := TFDCommand.Create(nil);
    Command.Connection := oConnection;

    Query := TFDQuery.Create(nil);
    Query.Connection := oConnection;
    Query.Open(' select * from pedido where id_pedido = '+IntToStr(PId_Pedido));
    if not Query.IsEmpty then
    begin
      try
        oConnection.StartTransaction;
        Command.Close;
        Command.CommandText.Text := ' delete from pedido_item where id_pedido = :id_pedido ';
        Command.Params.ParamByName('id_pedido').AsInteger := PId_Pedido;
        Command.Execute();
        //
        Command.Close;
        Command.CommandText.Text := ' delete from pedido where id_pedido = :id_pedido ';
        Command.Params.ParamByName('id_pedido').AsInteger := PId_Pedido;
        Command.Execute();
        oConnection.Commit;
        //
        Result := True;
      except
        on E:Exception do
        begin
          oConnection.Rollback;
          Result := False;
        end;
      end;
    end
    else
    begin
      Result := False;
    end;
  finally
    Command.Free;
    Query.Free;
  end;
end;

function TCtrPedido.ProcuraPedido(PId_Pedido: Integer): TPedido;
var
  ObjPedido: TPedido;
  ObjPedidoItem: TPedidoItem;
  Query, Query2 :TFDQuery;
  oConnection:TFDConnection;
begin
  try
    oConnection       := GetConnection;
    Query             := TFDQuery.Create(nil);
    Query2            := TFDQuery.Create(nil);
    Query.Connection  := oConnection;
    Query2.Connection := oConnection;
    Query.SQL.Text    := ' select pe.*, c.nome_cli '+
                         ' from pedido pe '+
                         ' left join clientes c on c.id_cliente = pe.id_cliente '+
                         ' where pe.id_pedido = '+IntToStr(PId_Pedido);
    Query.Open;
    ObjPedido             := TPedido.Create();
    ObjPedido.id_pedido   := Query.FieldByName('id_pedido').AsInteger;
    ObjPedido.id_cliente  := Query.FieldByName('id_cliente').AsInteger;
    ObjPedido.nome_cli    := Query.FieldByName('nome_cli').AsString;
    ObjPedido.valor_total := Query.FieldByName('valor_total').AsFloat;
    //
    Query.SQL.Text    := ' select pi.*, p.nome_prod from pedido_item pi '+
                         ' inner join produtos p on p.id_produto = pi.id_produto '+
                         ' where pi.id_pedido = '+IntToStr(PId_Pedido);
    Query.open;
    while not Query.Eof do
    begin
      ObjPedidoItem             := TPedidoItem.Create;
      ObjPedidoItem.id_pedido   := Query.FieldByName('id_pedido').AsInteger;
      ObjPedidoItem.id_item     := Query.FieldByName('id_item').AsInteger;
      ObjPedidoItem.id_produto  := Query.FieldByName('id_produto').AsInteger;
      ObjPedidoItem.nome_prod   := Query.FieldByName('nome_prod').AsString;
      ObjPedidoItem.qtde        := Query.FieldByName('qtde').AsInteger;
      ObjPedidoItem.valor_unit  := Query.FieldByName('valor_unit').AsFloat;
      ObjPedidoItem.valor_total := Query.FieldByName('valor_total').AsFloat;
      //
      ObjPedido.listapedidoitem.Add(ObjPedidoItem);
      Query.Next;
    end;
    Result := ObjPedido;
  finally
    Query.Free;
    oConnection.Close;
    FreeAndNil(oConnection);
  end;
  Result:= ObjPedido;
end;




end.
