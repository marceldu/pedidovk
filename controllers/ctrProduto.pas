unit ctrProduto;

interface

uses
  Data.DB,
  uDM,
  mdlProduto,
  System.Classes,
  System.SysUtils,
  //
{

//tirar depois !!! ///////////////////////////////////////

  FireDAC.Stan.Intf,
  FireDAC.Stan.Option,
  FireDAC.Stan.Error,
  FireDAC.UI.Intf,
  FireDAC.Phys.Intf,
  FireDAC.Stan.Def,
  FireDAC.Stan.Pool,
  FireDAC.Stan.Async,
  FireDAC.Phys,
  FireDAC.Phys.FB,
  FireDAC.Phys.FBDef,
  FireDAC.VCLUI.Wait,
  FireDAC.Stan.Param,
  FireDAC.DatS,
  FireDAC.DApt.Intf,
}
  FireDAC.DApt,
  FireDAC.Comp.DataSet,
  //
  FireDAC.Comp.Client;





type
  TCtrProduto = class
  private
    { private declarations }
  protected
    { protected declarations }

  public
    { public declarations }
    function GetConnection:TFDConnection;
    function ProcuraProdID(PCodProd: Integer): TProduto;

{
    function ProcuraTodosProdTipo(PCodTipoProd: Integer): TObjectList<TProdutoEnt>;
    function ProcuraTodosProd: TObjectList<TProdutoEnt>;
    function ExcluiProd(PCodProd: Integer): integer;
    function InsereProd(ProdutoEnt: TProdutoEnt): Integer;
    function AlteraProd(iCodProdAnt: integer; ProdutoEnt: TProdutoEnt): Integer;
}
  published
    { published declarations }
  end;


implementation


function TCtrProduto.GetConnection: TFDConnection;
begin
    DM := TDM.Create(nil);
    Result:= DM.FDConnection;
end;

function TCtrProduto.ProcuraProdID(PCodProd: Integer): TProduto;
var
  Obj: TProduto;
  Query: TFDQuery;
  oConnection: TFDConnection;
begin
  try
    oConnection      := GetConnection;
    Query            := TFDQuery.Create(nil);
    Query.Connection := oConnection;
    Query.SQL.Text   := 'select * from produtos where id_produto = '+PCodProd.ToString;
    Query.Open();

    Obj            := TProduto.Create;
    Obj.id_produto := Query.FieldByName('id_produto').AsInteger;
    Obj.nome_prod  := Query.FieldByName('nome_prod').AsString;
    Obj.preco_prod := Query.FieldByName('preco_prod').AsFloat;

  finally
    Query.Free;
    oConnection.Close;
    FreeAndNil(oConnection);
  end;
  Result:= Obj;
end;

end.
