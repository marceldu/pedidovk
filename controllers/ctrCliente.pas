unit ctrCliente;

interface

uses
  FireDAC.Comp.Client,
  Data.DB,
  uDM,
  mdlCliente,
  System.Classes,
  System.SysUtils;


type
  TCtrCliente = class
  private
    { private declarations }
  protected
    { protected declarations }

  public
    { public declarations }
    function GetConnection:TFDConnection;
    function ProcuraCliID(PCodCli: Integer): TCliente;

  published
    { published declarations }
  end;


implementation


function TCtrCliente.GetConnection: TFDConnection;
begin
    DM := TDM.Create(nil);
    Result:= DM.FDConnection;
end;

function TCtrCliente.ProcuraCliID(PCodCli: Integer): TCliente;
var
  Obj: TCliente;
  Query: TFDQuery;
  oConnection: TFDConnection;
begin
  try
    oConnection      := GetConnection;
    Query            := TFDQuery.Create(nil);
    Query.Connection := oConnection;
    Query.SQL.Text   := 'select * from clientes where id_cliente = '+PCodCli.ToString;
    Query.Open();

    Obj            := TCliente.Create;
    Obj.id_cliente := Query.FieldByName('id_cliente').AsInteger;
    Obj.nome_cli   := Query.FieldByName('nome_cli').AsString;
    Obj.cidade_cli := Query.FieldByName('cidade_cli').AsString;
    Obj.uf_cli     := Query.FieldByName('uf_cli').AsString;

  finally
    Query.Free;
    oConnection.Close;
    FreeAndNil(oConnection);
  end;
  Result:= Obj;
end;

end.
