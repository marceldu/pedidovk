unit vwPedido;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.MySQL,
  FireDAC.Phys.MySQLDef, FireDAC.VCLUI.Wait, Vcl.StdCtrls, FireDAC.Comp.Client,
  Data.DB, Data.Win.ADODB, Datasnap.DBClient, Vcl.Grids, Vcl.DBGrids,
  Vcl.ExtCtrls, System.Actions, Vcl.ActnList, Vcl.Buttons, mdlPedidoItem,
  mdlPedido, ctrPedido, Vcl.Mask, Vcl.DBCtrls;

type
  TfPedido = class(TForm)
    btnBuscaCli: TButton;
    cdsPedido: TClientDataSet;
    cdsPedidoid_pedido: TIntegerField;
    cdsPedidoid_cliente: TIntegerField;
    cdsPedidodata_emissao: TDateField;
    cdsPedidovalor_total: TBCDField;
    cdsPedidoItem: TClientDataSet;
    cdsPedidoItemid_pedido: TIntegerField;
    cdsPedidoItemid_item: TAutoIncField;
    cdsPedidoItemid_produto: TIntegerField;
    cdsPedidoItemqtde: TIntegerField;
    cdsPedidoItemvalor_unit: TBCDField;
    cdsPedidoItemvalor_total: TBCDField;
    dsPedido: TDataSource;
    dsPedidoItem: TDataSource;
    DBGrid1: TDBGrid;
    cdsPedidoItemnome_prod: TStringField;
    cdsPedidonome_cli: TStringField;
    Label1: TLabel;
    Label2: TLabel;
    pnlProduto: TPanel;
    btnBuscaProd: TButton;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    btnInserirProd: TButton;
    btnAlterarProd: TButton;
    btnExcluirProd: TButton;
    ActionList1: TActionList;
    InserirProduto: TAction;
    AlterarProduto: TAction;
    ExcluirProduto: TAction;
    btnGravaPedido: TBitBtn;
    btnNovoPedido: TBitBtn;
    btnConfirmarProd: TButton;
    btnCancelarProd: TButton;
    edtIDProduto: TDBEdit;
    edtNomeProd: TDBEdit;
    edtQtde: TDBEdit;
    edtValorUnit: TDBEdit;
    edtValorTotal: TDBEdit;
    edtNomeCli: TDBEdit;
    edtIDCliente: TDBEdit;
    Label8: TLabel;
    edtTotalPedido: TEdit;
    btnCancelarPed: TBitBtn;
    Panel1: TPanel;
    btnBuscarPed: TBitBtn;
    edtIdPedido: TEdit;
    Label9: TLabel;
    function  fProcuraProd(id: integer): boolean;
    procedure btnBuscaProdClick(Sender: TObject);
    procedure btnBuscaCliClick(Sender: TObject);
    function  fProcuraCli(id: integer): Boolean;
    procedure InserirProdutoExecute(Sender: TObject);
    procedure AlterarProdutoExecute(Sender: TObject);
    procedure ExcluirProdutoExecute(Sender: TObject);
    procedure btnNovoPedidoClick(Sender: TObject);
    procedure btnCancelarProdClick(Sender: TObject);
    procedure btnConfirmarProdClick(Sender: TObject);
    function  fCalculaTotal: Double;
    procedure btnGravaPedidoClick(Sender: TObject);
    function fInserePedido: Boolean;
    procedure FormShow(Sender: TObject);
    procedure DBGrid1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtIDClienteKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtIDProdutoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtQtdeKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtValorUnitKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btnBuscarPedClick(Sender: TObject);
    procedure btnCancelarPedClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fPedido: TfPedido;

implementation

{$R *.dfm}

uses ctrProduto, mdlProduto, ctrCliente, mdlCliente;

procedure TfPedido.btnBuscaProdClick(Sender: TObject);
begin
  if not (cdsPedidoItem.State in [dsInsert]) then
    Exit;
  if fProcuraProd(StrToInt(edtIDProduto.Text)) then
  begin
    edtQtde.SetFocus;
    edtQtde.SelectAll;
  end
  else
  begin
    edtIDProduto.SetFocus;
    edtIDProduto.SelectAll;
  end;
end;

procedure TfPedido.btnBuscarPedClick(Sender: TObject);
var
  Pedido: TPedido;
  CtrPedido: TCtrPedido;
  ObjPedidoItem: TPedidoItem;
  i: integer;
begin
  CtrPedido := TCtrPedido.Create();

  Pedido := TPedido.Create();
  Pedido := CtrPedido.ProcuraPedido(StrToInt(edtIdPedido.Text));

  // Grava dados do pedido nas tabelas temporiarias

  cdsPedido.EmptyDataSet;
  cdsPedidoItem.EmptyDataSet;
  //
  cdsPedido.Append;
  cdsPedidoid_pedido.AsInteger := Pedido.id_pedido;
  cdsPedidoid_cliente.AsInteger := Pedido.id_cliente;
  cdsPedidonome_cli.AsString := Pedido.nome_cli;
  cdsPedidodata_emissao.AsDateTime := Pedido.data_emissao;
  cdsPedidovalor_total.Value := Pedido.valor_total;
  cdsPedido.Post;

  //

  for I := 0 to Pedido.listapedidoitem.Count - 1  do
  begin
    cdsPedidoItem.Append;
    cdsPedidoItemid_pedido.AsInteger  := Pedido.listapedidoitem[i].id_pedido;
//    cdsPedidoItemid_item.AsInteger    := Pedido.listapedidoitem[i].id_item;
    cdsPedidoItemid_produto.AsInteger := Pedido.listapedidoitem[i].id_produto;
    cdsPedidoItemnome_prod.AsString   := Pedido.listapedidoitem[i].nome_prod;
    cdsPedidoItemqtde.AsInteger       := Pedido.listapedidoitem[i].qtde;
    cdsPedidoItemvalor_unit.AsFloat   := Pedido.listapedidoitem[i].valor_unit;
    cdsPedidoItemvalor_total.AsFloat  := Pedido.listapedidoitem[i].valor_total;
    cdsPedidoItem.Post;
  end;


end;

procedure TfPedido.btnConfirmarProdClick(Sender: TObject);
begin
  if cdsPedidoItem.State in [dsInsert, dsEdit] then
    cdsPedidoItem.Post;
  //
  edtTotalPedido.Text :=  FormatFloat('###,##0.00', fCalculaTotal);
  pnlProduto.Color    := clGray;
  pnlProduto.Enabled  := False;
  btnInserirProd.SetFocus;
end;

function TfPedido.fCalculaTotal: Double;
var
  bMark: TBookMark;
  dValor: Double;
begin
  bMark  := cdsPedidoItem.GetBookmark;
  dValor := 0;
  cdsPedidoItem.DisableControls;
  cdsPedidoItem.first;
  while not cdsPedidoItem.Eof do
  begin
    dValor := dValor + cdsPedidoItemvalor_total.AsFloat;
    cdsPedidoItem.Next;
  end;
  cdsPedidoItem.GotoBookmark(bMark);
  cdsPedidoItem.EnableControls;
  Result := dValor;
end;

procedure TfPedido.btnCancelarProdClick(Sender: TObject);
begin
  pnlProduto.Color   := clGray;
  pnlProduto.Enabled := False;
  btnInserirProd.SetFocus;
end;

procedure TfPedido.edtValorUnitKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (key = vk_return) then
  begin
    cdsPedidoItemvalor_total.AsFloat := cdsPedidoItemqtde.AsInteger * cdsPedidoItemvalor_unit.AsFloat;
    btnConfirmarProd.SetFocus;
  end;
end;

procedure TfPedido.DBGrid1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key = vk_delete then
    btnExcluirProd.Click;
  if (key = vk_return) then
    btnAlterarProd.Click;
end;

procedure TfPedido.InserirProdutoExecute(Sender: TObject);
begin
  pnlProduto.color   := $00EAFFFF;
  pnlProduto.Enabled := True;
  cdsPedidoItem.Append;
  edtIDProduto.SetFocus;
  edtIDProduto.ReadOnly := False;
  edtIDProduto.Color    := clWhite;
end;

procedure TfPedido.AlterarProdutoExecute(Sender: TObject);
begin
  if cdsPedidoItem.IsEmpty then
  begin
    showmessage('Nenhum registro encontrado para altera��o.');
    Exit;
  end;
  cdsPedidoItem.Edit;
  //
  pnlProduto.color      := $00EAFFFF;
  pnlProduto.Enabled    := True;
  edtIDProduto.ReadOnly := True;
  edtIDProduto.Color    := clSilver;
  //
  edtQtde.SetFocus;
  edtQtde.SelectAll;
end;

procedure TfPedido.btnGravaPedidoClick(Sender: TObject);
begin

  if fInserePedido then
    showmessage('Pedido inserido com sucesso.');
  //
  cdsPedido.EmptyDataSet;
  cdsPedidoItem.EmptyDataSet;
  btnNovoPedido.SetFocus;
end;

procedure TfPedido.btnCancelarPedClick(Sender: TObject);
var
  CtrPedido: TCtrPedido;
begin
  if cdsPedido.IsEmpty then
  begin
    showmessage('Nenhum pedido encontrado para exclus�o.');
    Exit;
  end;
  if MessageDlg('Deseja excluir do pedido ?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
  begin
    CtrPedido := TCtrPedido.Create;
    if CtrPedido.ExcluiPedido(cdsPedidoid_pedido.AsInteger) then
      showmessage('Pedido cancelado com sucesso.');
    //
    cdsPedido.EmptyDataSet;
    cdsPedidoItem.EmptyDataSet;
    btnNovoPedido.SetFocus;
  end;
end;


procedure TfPedido.btnNovoPedidoClick(Sender: TObject);
begin
  cdsPedido.EmptyDataSet;
  cdsPedido.Append;
  edtIDCliente.SetFocus;
  edtIDCliente.SelectAll;
end;

procedure TfPedido.btnBuscaCliClick(Sender: TObject);
begin
  if fProcuraCli(StrToInt(edtIDCliente.Text)) then
    btnInserirProd.SetFocus
  else
  begin
    edtIDCliente.SetFocus;
    edtIDCliente.SelectAll;
  end;
end;

procedure TfPedido.edtIDClienteKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (key = vk_return) then
    btnBuscaCli.Click;
end;

procedure TfPedido.edtIDProdutoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if ( Key = vk_return ) then
    btnBuscaProd.Click;
end;

procedure TfPedido.edtQtdeKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if ( Key = vk_return ) then
  begin
    cdsPedidoItemvalor_total.AsFloat := cdsPedidoItemqtde.AsInteger * cdsPedidoItemvalor_unit.AsFloat;
    edtValorUnit.SetFocus;
    edtValorUnit.SelectAll;
  end;
end;

procedure TfPedido.ExcluirProdutoExecute(Sender: TObject);
begin
  if cdsPedidoItem.IsEmpty then
  begin
    showmessage('Nenhum registro encontrado para exclus�o.');
    Exit;
  end;
  if MessageDlg('Deseja excluir da produto ?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
  begin
    cdsPedidoItem.Delete;
    edtTotalPedido.Text := FormatFloat('###,##0.00', fCalculaTotal);
    cdsPedidoItem.First;
  end;
end;

function TfPedido.fProcuraCli(id: integer): Boolean;
var
  CtrlCliente: TCtrCliente;
  Cliente: TCliente;
begin
  Result := False;
  CtrlCliente := TCtrCliente.Create();
  try
    Cliente := CtrlCliente.ProcuraCliID(id);
    if Cliente.id_cliente.ToString = '' then
    begin
      showmessage('cliente n�o encontrado.');
      cdsPedidonome_cli.Clear;
    end
    else
    begin
      cdsPedidonome_cli.AsString := Cliente.nome_cli;
      Result                     := True;
    end;
  finally
    CtrlCliente.Free;
    Cliente.Free;
  end;
end;

function TfPedido.fProcuraProd(id: integer): boolean;
var
  CtrlProduto: TCtrProduto;
  Produto: TProduto;
begin
  Result := False;
  CtrlProduto := TCtrProduto.Create();
  try
    Produto := CtrlProduto.ProcuraProdID(id);
    if Produto.id_produto.ToString = '' then
    begin
      showmessage('produto n�o encontrado.');
      cdsPedidoItemnome_prod.Clear;
      edtValorUnit.Text  := '0';
      edtValorTotal.Text := '0';
    end
    else
    begin
      cdsPedidoItemnome_prod.AsString  := Produto.nome_prod;
      cdsPedidoItemqtde.AsInteger      := 1;
      cdsPedidoItemvalor_unit.AsFloat  := Produto.preco_prod;
      cdsPedidoItemvalor_total.AsFloat := cdsPedidoItemqtde.AsInteger * cdsPedidoItemvalor_unit.AsFloat;
      Result                           := True;
    end;
  finally
    CtrlProduto.Free;
  end;
end;

function TfPedido.fInserePedido: Boolean;
var
  Pedido: TPedido;
  Cliente: TCliente;
  CtrPedido: TCtrPedido;
  dValorTotal: Double;
  i: integer;
begin
  Pedido := TPedido.Create();
  Pedido.id_pedido  := 0;
  Pedido.id_cliente := StrToInt(edtIDCliente.Text);
  Pedido.data_emissao := Trunc(now);
  //
  dValorTotal := 0;
  //
  cdsPedidoItem.First;
  while not cdsPedidoItem.Eof do
  begin
    /////////////
    Pedido.listapedidoitem.Add(TPedidoItem.Create);
    i := Pedido.listapedidoitem.Count - 1;
    Pedido.listapedidoitem[i].id_produto  := cdsPedidoItemid_produto.AsInteger;
    Pedido.listapedidoitem[i].qtde        := cdsPedidoItemqtde.AsInteger;
    Pedido.listapedidoitem[i].valor_unit  := cdsPedidoItemvalor_unit.AsFloat;
    Pedido.listapedidoitem[i].valor_total := cdsPedidoItemvalor_total.AsFloat;
     /////////////
    //
    dValorTotal := dValorTotal + cdsPedidoItemvalor_total.AsFloat;
    cdsPedidoItem.Next;
  end;
  Pedido.valor_total := dValorTotal;
  //
  CtrPedido := TCtrPedido.Create;
  CtrPedido.InserePedido(Pedido)
end;


procedure TfPedido.FormShow(Sender: TObject);
begin
  btnNovoPedido.SetFocus;
end;

end.
