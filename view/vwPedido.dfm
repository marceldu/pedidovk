object fPedido: TfPedido
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'Pedidos'
  ClientHeight = 535
  ClientWidth = 661
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 18
    Top = 66
    Width = 46
    Height = 13
    Caption = 'Id Cliente'
  end
  object Label2: TLabel
    Left = 167
    Top = 65
    Width = 78
    Height = 13
    Caption = 'Nome do Cliente'
  end
  object Label8: TLabel
    Left = 328
    Top = 440
    Width = 185
    Height = 19
    Caption = 'Total do Pedido (R$)...'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object btnBuscaCli: TButton
    Left = 70
    Top = 78
    Width = 91
    Height = 25
    Caption = 'Busca Cliente'
    TabOrder = 1
    OnClick = btnBuscaCliClick
  end
  object DBGrid1: TDBGrid
    Left = 16
    Top = 227
    Width = 625
    Height = 204
    DataSource = dsPedidoItem
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
    ReadOnly = True
    TabOrder = 7
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    OnKeyDown = DBGrid1KeyDown
    Columns = <
      item
        Expanded = False
        FieldName = 'id_produto'
        Title.Caption = 'id produto'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'nome_prod'
        Title.Caption = 'nome prod'
        Width = 308
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'qtde'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'valor_unit'
        Title.Caption = 'valor unit (R$)'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'valor_total'
        Title.Caption = 'valor total (R$)'
        Visible = True
      end>
  end
  object pnlProduto: TPanel
    Left = 16
    Top = 110
    Width = 625
    Height = 82
    BevelInner = bvLowered
    Color = clGray
    ParentBackground = False
    TabOrder = 3
    object Label3: TLabel
      Left = 151
      Top = 10
      Width = 83
      Height = 13
      Caption = 'Nome do Produto'
    end
    object Label4: TLabel
      Left = 17
      Top = 10
      Width = 35
      Height = 13
      Caption = 'Id Prod'
    end
    object Label5: TLabel
      Left = 399
      Top = 10
      Width = 24
      Height = 13
      Caption = 'Qtde'
    end
    object Label6: TLabel
      Left = 445
      Top = 10
      Width = 74
      Height = 13
      Caption = 'Valor Unit. (R$)'
    end
    object Label7: TLabel
      Left = 529
      Top = 10
      Width = 71
      Height = 13
      Caption = 'Valor Tot. (R$)'
    end
    object btnBuscaProd: TButton
      Left = 62
      Top = 21
      Width = 83
      Height = 25
      Caption = 'Busca Produto'
      TabOrder = 1
      OnClick = btnBuscaProdClick
    end
    object btnConfirmarProd: TButton
      Left = 404
      Top = 52
      Width = 100
      Height = 25
      Caption = 'Confirmar'
      TabOrder = 6
      OnClick = btnConfirmarProdClick
    end
    object btnCancelarProd: TButton
      Left = 508
      Top = 52
      Width = 100
      Height = 25
      Caption = 'Cancelar'
      TabOrder = 7
      OnClick = btnCancelarProdClick
    end
    object edtIDProduto: TDBEdit
      Left = 17
      Top = 24
      Width = 40
      Height = 21
      DataField = 'id_produto'
      DataSource = dsPedidoItem
      TabOrder = 0
      OnKeyDown = edtIDProdutoKeyDown
    end
    object edtNomeProd: TDBEdit
      Left = 151
      Top = 24
      Width = 240
      Height = 21
      TabStop = False
      Color = clSilver
      DataField = 'nome_prod'
      DataSource = dsPedidoItem
      TabOrder = 2
    end
    object edtQtde: TDBEdit
      Left = 399
      Top = 24
      Width = 39
      Height = 21
      DataField = 'qtde'
      DataSource = dsPedidoItem
      TabOrder = 3
      OnKeyDown = edtQtdeKeyDown
    end
    object edtValorUnit: TDBEdit
      Left = 445
      Top = 24
      Width = 77
      Height = 21
      DataField = 'valor_unit'
      DataSource = dsPedidoItem
      TabOrder = 4
      OnKeyDown = edtValorUnitKeyDown
    end
    object edtValorTotal: TDBEdit
      Left = 528
      Top = 25
      Width = 77
      Height = 21
      TabStop = False
      Color = clSilver
      DataField = 'valor_total'
      DataSource = dsPedidoItem
      TabOrder = 5
    end
  end
  object btnInserirProd: TButton
    Left = 336
    Top = 197
    Width = 100
    Height = 25
    Action = InserirProduto
    TabOrder = 4
  end
  object btnAlterarProd: TButton
    Left = 440
    Top = 197
    Width = 100
    Height = 25
    Action = AlterarProduto
    TabOrder = 5
  end
  object btnExcluirProd: TButton
    Left = 543
    Top = 197
    Width = 100
    Height = 25
    Action = ExcluirProduto
    TabOrder = 6
  end
  object btnGravaPedido: TBitBtn
    Left = 333
    Top = 486
    Width = 153
    Height = 41
    Caption = 'Gravar Pedido'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 9
    OnClick = btnGravaPedidoClick
  end
  object btnNovoPedido: TBitBtn
    Left = 174
    Top = 486
    Width = 153
    Height = 41
    Caption = 'Novo Pedido'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 8
    OnClick = btnNovoPedidoClick
  end
  object edtNomeCli: TDBEdit
    Left = 167
    Top = 80
    Width = 474
    Height = 21
    TabStop = False
    Color = clSilver
    DataField = 'nome_cli'
    DataSource = dsPedido
    TabOrder = 2
  end
  object edtIDCliente: TDBEdit
    Left = 18
    Top = 80
    Width = 48
    Height = 21
    DataField = 'id_cliente'
    DataSource = dsPedido
    TabOrder = 0
    OnKeyDown = edtIDClienteKeyDown
  end
  object edtTotalPedido: TEdit
    Left = 520
    Top = 438
    Width = 121
    Height = 27
    Alignment = taRightJustify
    Color = clSilver
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 10
    Text = '0,00'
  end
  object btnCancelarPed: TBitBtn
    Left = 492
    Top = 486
    Width = 153
    Height = 41
    Caption = 'Cancelar Pedido'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 11
    OnClick = btnCancelarPedClick
  end
  object Panel1: TPanel
    Left = 18
    Top = 8
    Width = 185
    Height = 53
    Color = 15400959
    ParentBackground = False
    TabOrder = 12
    object Label9: TLabel
      Left = 18
      Top = 8
      Width = 45
      Height = 13
      Caption = 'Id Pedido'
    end
    object btnBuscarPed: TBitBtn
      Left = 79
      Top = 16
      Width = 94
      Height = 28
      Caption = 'Buscar Pedido'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      OnClick = btnBuscarPedClick
    end
    object edtIdPedido: TEdit
      Left = 16
      Top = 22
      Width = 57
      Height = 21
      TabOrder = 1
    end
  end
  object cdsPedido: TClientDataSet
    PersistDataPacket.Data = {
      A00000009619E0BD010000001800000005000000000003000000A0000969645F
      70656469646F04000100000000000A69645F636C69656E746504000100000000
      000C646174615F656D697373616F04000600000000000B76616C6F725F746F74
      616C070005000000020008444543494D414C5302000200020005574944544802
      0002000A00086E6F6D655F636C69010049000000010005574944544802000200
      64000000}
    Active = True
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'id_pedido'
        DataType = ftInteger
      end
      item
        Name = 'id_cliente'
        DataType = ftInteger
      end
      item
        Name = 'data_emissao'
        DataType = ftDate
      end
      item
        Name = 'valor_total'
        DataType = ftBCD
        Precision = 10
        Size = 2
      end
      item
        Name = 'nome_cli'
        DataType = ftString
        Size = 100
      end>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    Left = 181
    Top = 270
    object cdsPedidoid_pedido: TIntegerField
      FieldName = 'id_pedido'
    end
    object cdsPedidoid_cliente: TIntegerField
      FieldName = 'id_cliente'
    end
    object cdsPedidodata_emissao: TDateField
      FieldName = 'data_emissao'
    end
    object cdsPedidovalor_total: TBCDField
      FieldName = 'valor_total'
      Precision = 10
      Size = 2
    end
    object cdsPedidonome_cli: TStringField
      FieldName = 'nome_cli'
      Size = 100
    end
  end
  object cdsPedidoItem: TClientDataSet
    PersistDataPacket.Data = {
      D70000009619E0BD010000001800000007000000000003000000D7000969645F
      70656469646F04000100000000000769645F6974656D04000100000000000A69
      645F70726F6475746F0400010000000000047174646504000100000000000A76
      616C6F725F756E6974070005000000020008444543494D414C53020002000200
      055749445448020002000A000B76616C6F725F746F74616C0700050000000200
      08444543494D414C53020002000200055749445448020002000A00096E6F6D65
      5F70726F6401004900000001000557494454480200020064000000}
    Active = True
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'id_pedido'
        DataType = ftInteger
      end
      item
        Name = 'id_item'
        DataType = ftInteger
      end
      item
        Name = 'id_produto'
        DataType = ftInteger
      end
      item
        Name = 'qtde'
        DataType = ftInteger
      end
      item
        Name = 'valor_unit'
        DataType = ftBCD
        Precision = 10
        Size = 2
      end
      item
        Name = 'valor_total'
        DataType = ftBCD
        Precision = 10
        Size = 2
      end
      item
        Name = 'nome_prod'
        DataType = ftString
        Size = 100
      end>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    Left = 365
    Top = 270
    object cdsPedidoItemid_pedido: TIntegerField
      FieldName = 'id_pedido'
    end
    object cdsPedidoItemid_item: TAutoIncField
      FieldName = 'id_item'
      ReadOnly = True
    end
    object cdsPedidoItemid_produto: TIntegerField
      FieldName = 'id_produto'
    end
    object cdsPedidoItemqtde: TIntegerField
      FieldName = 'qtde'
    end
    object cdsPedidoItemvalor_unit: TBCDField
      FieldName = 'valor_unit'
      Precision = 10
      Size = 2
    end
    object cdsPedidoItemvalor_total: TBCDField
      FieldName = 'valor_total'
      Precision = 10
      Size = 2
    end
    object cdsPedidoItemnome_prod: TStringField
      FieldName = 'nome_prod'
      Size = 100
    end
  end
  object dsPedido: TDataSource
    DataSet = cdsPedido
    Left = 184
    Top = 318
  end
  object dsPedidoItem: TDataSource
    DataSet = cdsPedidoItem
    Left = 368
    Top = 318
  end
  object ActionList1: TActionList
    Left = 528
    Top = 334
    object InserirProduto: TAction
      Caption = 'Inserir Produto'
      OnExecute = InserirProdutoExecute
    end
    object AlterarProduto: TAction
      Caption = 'Alterar Produto'
      OnExecute = AlterarProdutoExecute
    end
    object ExcluirProduto: TAction
      Caption = 'Excluir Produto'
      OnExecute = ExcluirProdutoExecute
    end
  end
end
