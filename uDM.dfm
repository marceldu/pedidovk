object DM: TDM
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 453
  Width = 597
  object FDConnection: TFDConnection
    Params.Strings = (
      'Database=dbteste'
      'User_Name=root'
      'Password=newrootpassword'
      'Server=localhost'
      'DriverID=MySQL')
    Connected = True
    LoginPrompt = False
    Left = 64
    Top = 24
  end
  object FDPhysMySQLDriverLink: TFDPhysMySQLDriverLink
    VendorLib = 
      'C:\Projetos\Desenvolvimento\Delphi\ProjetoVendaWKTech\lib32\libm' +
      'ysql.dll'
    Left = 64
    Top = 80
  end
  object FDQuery1: TFDQuery
    Connection = FDConnection
    SQL.Strings = (
      'select * from pedido_item')
    Left = 280
    Top = 256
    object FDQuery1id_pedido: TIntegerField
      FieldName = 'id_pedido'
      Origin = 'id_pedido'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object FDQuery1id_item: TFDAutoIncField
      FieldName = 'id_item'
      Origin = 'id_item'
      ProviderFlags = [pfInWhere, pfInKey]
    end
    object FDQuery1id_produto: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'id_produto'
      Origin = 'id_produto'
    end
    object FDQuery1qtde: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'qtde'
      Origin = 'qtde'
    end
    object FDQuery1valor_unit: TBCDField
      AutoGenerateValue = arDefault
      FieldName = 'valor_unit'
      Origin = 'valor_unit'
      Precision = 10
      Size = 2
    end
    object FDQuery1valor_total: TBCDField
      AutoGenerateValue = arDefault
      FieldName = 'valor_total'
      Origin = 'valor_total'
      Precision = 10
      Size = 2
    end
  end
  object DataSetProvider1: TDataSetProvider
    DataSet = FDQuery1
    Left = 280
    Top = 208
  end
end
