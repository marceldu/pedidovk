unit uDM;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.MySQL,
  FireDAC.Phys.MySQLDef, FireDAC.VCLUI.Wait, Data.DB, FireDAC.Comp.Client,
  FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf, FireDAC.DApt,
  FireDAC.Comp.DataSet, Datasnap.Provider;

type
  TDM = class(TDataModule)
    FDConnection: TFDConnection;
    FDPhysMySQLDriverLink: TFDPhysMySQLDriverLink;
    FDQuery1: TFDQuery;
    DataSetProvider1: TDataSetProvider;
    FDQuery1id_pedido: TIntegerField;
    FDQuery1id_item: TFDAutoIncField;
    FDQuery1id_produto: TIntegerField;
    FDQuery1qtde: TIntegerField;
    FDQuery1valor_unit: TBCDField;
    FDQuery1valor_total: TBCDField;
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DM: TDM;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

procedure TDM.DataModuleCreate(Sender: TObject);
begin

  FDConnection.Close;
  //
  FDConnection.Params.Clear;
  // apontamento servidor local /////////////////////////
  FDConnection.Params.Add('Database=dbTeste');
  FDConnection.Params.Add('User_Name=root');
  FDConnection.Params.Add('Password=newrootpassword');
  FDConnection.Params.Add('Server=localhost');
  FDConnection.Params.Add('DriverID=MySQL');
  FDConnection.Open();

end;

end.
